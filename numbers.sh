#==========================================
# Game - Numbers
# V-1.0
# 8 March 2022
#==========================================

#!/bin/bash
echo "================================="
echo "WELCOME TO the game NUMBERS!"
echo "================================="
echo

x=50
y=50

echo "Please choose a number between 1 and 100"
echo

read -p "Ready?? (press Enter to start)" n

while true
do
    read -p "Is your number bigger then $x? (+\-)        (if that is you number press '!')  " answ
    if [ $answ = "!" ]; then
        echo
        echo "   WIN WIN WIN!!!! CONGRATULATION!!!!!!!!!*&^%$#@#$%^&"
        break
    elif [ $answ = "+" ]; then
        echo "bigger"
        y=$((y/2))
        x=$((x+y))
    elif [ $answ = "-" ]; then
        echo "smaller"
        y=$((y/2))
        x=$((x-y))
    else echo "Please, enter correct answer ^"
    fi
    if [ $((y%2)) = 1 ] && [ $y != 25 ] && [ $y != 1 ]; then
        let y++
    fi
    echo
done
