#==========================================
# Genereting Passweod tool
# V-1.0
# 5 March 2022
#==========================================

#!/bin/bash
echo "================================="
echo "WELCOME TO the PASSWORD GENERATOR"
echo "================================="
echo

function question() {
    read -p "What length of password should be?": length
    read -p "Should it be with special characters? (y/n)": charact
}

function gen_pass() {  # funcion of Generating without special characters
    if [ $charact = "n" ]; then
        tr -dc 'A-Za-z0-9' </dev/urandom | head -c $length  ; echo		# Genereting WITHOUT speciasl characters
    else tr -dc 'A-Za-z0-9!@#$%^&*' </dev/urandom | head -c $length  ; echo 	# Genereting WITH specoasl characters
    fi
}

while true
do
    question  # Calling function with questions
    if ! [[ $length =~ ^[0-9]+$ ]]; then   # Checking if length is number
        echo
        echo "(!) Please, enter the correct length of password"
        echo
        continue
    elif [ "$charact" = "n" ] || [ "$charact" = "y" ]; then        # Checking is the answer is y or n
        echo "Genereting new password..............."
        while true
        do
            gen_pass    				      # Calling the generating function
            read -p "  Generate anoter? (y - yes / any key to exit)": count
            if [ $count != "y" ] || [ -z "$count" ]; then     # Genereting password a few times in a row
                break
            fi
        done
        break
    fi
    echo
    echo "(!) Please, enter correct answer on question about using characters ( ONLY 'y' or 'n')"
    echo
done
